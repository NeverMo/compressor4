#include "ari.h"

void compress_ari(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");
    
    /*Сейчас мы будем прижаты ко дну*/
    
    int used[MAX_ASCII];
    memset(used, 0, sizeof(used));
    
    size_t file_size = 0;
    int c;
    
    while ((c = getc(ifp)) != EOF) {
        used[c] = 1;
        file_size++;
    }
    
    if (file_size == 0) {
        fclose(ofp);
        fclose(ifp);
        return ;
    }
    
    unsigned char count = -1;
    
    for (int i = 0; i < MAX_ASCII; ++i) {
        count += used[i];
    }
    
    fwrite(&file_size, sizeof(file_size), 1, ofp);
    fwrite(&count, sizeof(count), 1, ofp);
    //Нужна инициализация, обновление, поиск элемента
    //fwrite(freq, sizeof(freq[0]), MAX_ASCII, ofp);
    table_preinit();

    for (int i = 0; i < MAX_ASCII; ++i) {
        if (used[i] != 0) {
            unsigned char t = i;
            fwrite(&t, sizeof(t), 1, ofp);
            table_add(t);
        }
    }

    table_init();

    fseek(ifp, 0, SEEK_SET);
    
    init_writing();
    
    hi = MAX_HI;
    lo = 0;
    over_bits = 0;
    
    while ((c = getc(ifp)) != EOF) {
        SYM p = get_probs((unsigned char) c);
        encode_sym(ofp, &p);  
        table_update((unsigned char) c, AGR);
    }
    flush_coder(ofp);
    
     
    fclose(ifp);
    fclose(ofp);
}

void encode_sym(FILE *ofp, SYM *s) {
    
    unsigned long long dst = (unsigned long long) (hi - lo) + 1;
    
    hi = lo + (unsigned long long) ((dst * s->right) / s->total - 1);
    lo = lo + (unsigned long long) ((dst * s->left) / s->total);
    
    while (1) {

        if ((hi & HALF) == (lo & HALF)) {
            write_over_bits(ofp, lo & HALF);
        } else if ((lo & QUATER) && !(hi & QUATER)) {
            over_bits++;
            lo &= QUATER_1;
            hi |= QUATER;
        } else {
            break;
        }
        
        hi <<= 1;
        hi |= 1;
        lo <<= 1;
        hi &= MAX_HI;
        lo &= MAX_HI;
    }
}

void write_over_bits(FILE *ofp, int bit)
{
    write_bit(ofp, bit);
    while (over_bits) {
        over_bits--;
        write_bit(ofp, !bit);
    } 
}

void flush_coder(FILE *ofp)
{
    over_bits++;
    write_over_bits(ofp, lo & QUATER);
    write_fflush(ofp);
}



void decompress_ari(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");
    
    
    size_t file_size = 0;

    int c = getc(ifp);
    if (c == EOF) {
        fclose(ifp);
        fclose(ofp);
        return ;
    }

    fseek(ifp, 0, SEEK_SET);
    if (fread(&file_size, sizeof(file_size), 1, ifp) != 1) {
        fprintf(stderr, "Read err filesize\n");
        exit(EXIT_FAILURE);
    }
    
    unsigned char count;
    if (fread(&count, sizeof(count), 1, ifp) != 1) {
        fprintf(stderr, "Read err count symbols\n");
        exit(EXIT_FAILURE);
    }
    
    table_preinit();

    for (int i = 0; i <= count; ++i) {
        unsigned char p;
        if (fread(&p, sizeof(p), 1, ifp) != 1) {
            fprintf(stderr, "Read err symbols\n");
            exit(EXIT_FAILURE);
        }
        table_add(p);
    }

    table_init();


       
    
    
    hi = MAX_HI;
    lo = 0;
    over_bits = 0;
    value = 0;
    init_reading();
    
    for (int i = 0; i < BIT_COUNT; ++i) {
        value <<= 1;
        value += read_bit(ifp);
    }
    
    for (int i = 0; i < file_size; ++i) {
        SYM s = get_probs(0);
        unsigned long long cum = find_cum_sum(s.total);
        c = find_sym(cum, &s);
        table_update(c, AGR);
        decode_sym(ifp, &s);
        putc(c, ofp);
    }


    fclose(ifp);
    fclose(ofp);
}

void decode_sym(FILE *ofp, SYM *s)
{
    unsigned long long dst = (unsigned long long) (hi - lo) + 1;
    hi = lo + (unsigned long long) ((dst * s->right) / s->total - 1);
    lo = lo + (unsigned long long) ((dst * s->left) / s->total);
    while (1) {
        if ((hi & HALF) == (lo & HALF)) {
            //pass
        } else if ((lo & QUATER) && !(hi & QUATER)) {
            //value -= QUATER;
            //lo -= QUATER;
            //hi-= QUATER;
            value ^= QUATER;
            lo &= QUATER_1;
            hi |= QUATER;
        } else {
            break;
        }
        value <<= 1;
        value += read_bit(ofp);
        hi <<= 1;
        hi |= 1;
        lo <<= 1;
        lo &= MAX_HI;
        hi &= MAX_HI;
        value &= MAX_HI;
    }
}

unsigned long long find_cum_sum(unsigned long long full) 
{
    unsigned long long dst = (unsigned long long) (hi - lo) + 1ull;
    unsigned long long cum = (unsigned long long) ((((unsigned long long) (value - lo) + 1) * full - 1) / dst);
    return cum;
}


