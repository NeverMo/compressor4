#include "write_bits.h"

void init_writing(void)
{
    cur_byte = write_buf;
    *cur_byte = 0;
    mask = MASK;
}

void write_bit(FILE *ofp, int bit)
{
    if (bit) {
        *cur_byte |= mask;        
    }
    mask >>= 1;
    if (!mask) {
        mask = MASK;
        cur_byte++;
        if (cur_byte == &write_buf[WRITE_BUF_SIZE]) {
            fwrite(write_buf, sizeof(write_buf[0]), sizeof(write_buf), ofp);
            cur_byte = write_buf; 
        }
        *cur_byte = 0;
    }
}

void write_fflush(FILE *ofp)
{
    size_t not_in = cur_byte - write_buf;
    not_in++;
    fwrite(write_buf, sizeof(write_buf[0]), not_in, ofp);
}
