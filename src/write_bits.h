#ifndef __ARI_WRITE_BITS_H__
#define __ARI_WRITE_BITS_H__

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

enum Bounders
{ 
    WRITE_BUF_SIZE = 1024, 
    MASK = 0x80 
};

static unsigned char write_buf[WRITE_BUF_SIZE];
static unsigned char *cur_byte;
static int mask;

void
init_writing(void);

void
write_bit(FILE *ofp, int bit);

void
write_fflush(FILE *ofp);

#endif 
