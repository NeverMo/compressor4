#include "read_bits.h"

int read_bit(FILE *ifp)
{
    if (!pos_bit) {
        pos_bit = CHAR_BIT;
        cur_byte++;
        pos_byte--;
        if (!pos_byte) {
            pos_byte = fread(read_buf, sizeof(read_buf[0]), READ_BUF_SIZE, ifp);
            cur_byte = read_buf;
            if (!pos_byte) {
                if (imagine_part) {
                    fprintf(stderr, "Err\n");
                    exit(EXIT_FAILURE);
                }
                imagine_part = !0;
                pos_byte = ADD_TO_BUF;
            }
           
        }
    }
    pos_bit--;
    int ans = *cur_byte >> (pos_bit);
    ans &= 1;
    return ans;
}


void init_reading(void)
{
    pos_bit = imagine_part = 0;
    pos_byte = 1;
    cur_byte = read_buf;
}
