#ifndef __ARI_H__
#define __ARI_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include "write_bits.h"
#include "read_bits.h"
#include "table_handler.h"

#ifndef __SYM_ELEM_ARI_H_
#define __SYM_ELEM_ARI_H_

enum
{
    MAX_ASCII = 256,
    BIT_COUNT = 32
};

typedef struct SYM
{
    unsigned long long left;
    unsigned long long right;
    unsigned long long total;
} SYM;

#endif

#ifndef __AGR_ARI_H_
#define __AGR_ARI_H_

enum 
{
    AGR = 20000
};

#endif

void compress_ari(char *ifile, char *ofile);
void decompress_ari(char *ifile, char *ofile);
void encode_sym(FILE *ofp, SYM *s);
void write_over_bits(FILE *ofp, int bit);
void flush_coder(FILE *ofp);
unsigned long long find_cum_sum(unsigned long long full);



static unsigned long long MAX_HI = (1ull << BIT_COUNT) - 1ull; // 0xFFFF = 0b1111|1111|1111|1111
static unsigned long long QUATER = (1ull << (BIT_COUNT - 2)); 
static unsigned long long HALF = (1ull << (BIT_COUNT - 1));
static unsigned long long QUATER_1 = (1ull << (BIT_COUNT - 2)) - 1ull;
static unsigned long long hi;
static unsigned long long lo;
static unsigned long long value;
static long long over_bits;


#endif
