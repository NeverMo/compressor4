#ifndef __ARI_READ_BITS_H_
#define __ARI_READ_BITS_H_

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

enum
{
    ADD_TO_BUF = 8,
    READ_BUF_SIZE = 1024
};

static unsigned char read_buf[READ_BUF_SIZE + ADD_TO_BUF];
static unsigned char *cur_byte;
static int pos_byte, pos_bit, imagine_part;

int
read_bit(FILE *ifp);

#endif
