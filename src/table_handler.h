#ifndef __TABLE_HANDLER_ARI_H_
#define __TABLE_HANDLER_ARI_H_

#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>

#ifndef __SYM_ELEM_ARI_H_
#define __SYM_ELEM_ARI_H_

enum
{
    MAX_ASCII = 256,
    BIT_COUNT = 32
};

typedef struct SYM
{
    unsigned long long left;
    unsigned long long right;
    unsigned long long total;
} SYM;

#endif

static unsigned long long MAX_FREQ = (1ull << (BIT_COUNT - 2)) - 1;
static unsigned long long freq[MAX_ASCII];
static unsigned long long cum_sum[MAX_ASCII + 1];

void table_add(unsigned char c);
void table_preinit(void); // Здесь мы просто обнуляем freq, cum_sum
void table_init(void); // Здесь происходит изначатьлый пересчет cum_sum
SYM get_probs(unsigned char c);
static void update_cum_sum(int l);
void table_update(unsigned char c, int agr);
unsigned char find_sym(unsigned long long cum, SYM *s);
#endif
