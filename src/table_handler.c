#include "table_handler.h"

void table_preinit(void) 
{
    memset(freq, 0, sizeof(freq));
    memset(cum_sum, 0, sizeof(cum_sum));
}

void table_add(unsigned char c)
{
    freq[(int) c] = 1;   
}

void table_init(void)
{
    update_cum_sum(0);
}

static void update_cum_sum(int l)
{
    for (int i = l + 1; i <= MAX_ASCII; ++i) {
            cum_sum[i] = cum_sum[i - 1] + freq[i - 1];
        }
}

void table_update(unsigned char c, int agr)
{
    freq[(int) c] += agr;
    if (cum_sum[MAX_ASCII] + agr > MAX_FREQ) {
        for (int i = 0; i <= MAX_ASCII; ++i) {
            freq[(int)c] = (freq[(int) c] + 1) >> 1;
    
        }
        table_init();
    } else {
        update_cum_sum(c);
            }
}

SYM get_probs(unsigned char c)
{
    SYM s = {cum_sum[c], cum_sum[c + 1], cum_sum[MAX_ASCII]};
    return s;
}

unsigned char find_sym(unsigned long long cum, SYM *s)
{
    int l = -1, r = MAX_ASCII + 1, mid;
    while (r - l > 1) {
        mid = (r + l) >> 1;
        if (cum >= cum_sum[mid]) {
            l = mid;
        } else {
            r = mid;
        }
    }
    if (r != MAX_ASCII && cum >= cum_sum[r] && cum < cum_sum[r + 1]) {
        s->left = cum_sum[r];
        s->right = cum_sum[r + 1];
        s->total = cum_sum[MAX_ASCII];
        return (unsigned char) r;
 
    } else if (l != -1 && cum >= cum_sum[l] && cum < cum_sum[l + 1]) {
        s->left = cum_sum[l];
        s->right = cum_sum[l + 1];
        s->total = cum_sum[MAX_ASCII];
        return (unsigned char) l;
    }
    /*
     * Slow variant in finding index of char
     *
     * There was linear serch
     *
    */
    
    fprintf(stderr, "Not_found sym\n");
    exit(EXIT_FAILURE);
}


